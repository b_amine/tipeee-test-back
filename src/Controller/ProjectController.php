<?php
namespace App\Controller;

use App\Form\ProjectType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

use App\Entity\Project;
use App\Service\ProjectManager;

class ProjectController extends AbstractController {

    private $projectManager;

    public function __construct(ProjectManager $projectManager)
    {
        $this->projectManager = $projectManager;
    }

    /**
     * @Route("/projects", methods={"POST"})
     */
    public function postProject(Request $request): Response
    {

        $project = $this->projectManager->createProject();

        // Create a form with a custom name to match old name ('form') and not break eventual existing calls to this endpoint
        $formFactory = $this->get('form.factory');
        $form = $formFactory->createNamed('form', ProjectType::class,$project);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // Serializer is called natively inside json() function
            return $this->json($project);
        }
        return $this->json(["error" => "An error occured"]);
    }
}
